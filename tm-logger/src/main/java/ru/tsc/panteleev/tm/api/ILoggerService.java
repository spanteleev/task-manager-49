package ru.tsc.panteleev.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.LogDTO;

public interface ILoggerService {

    void writeLog(@NotNull LogDTO message);

}
