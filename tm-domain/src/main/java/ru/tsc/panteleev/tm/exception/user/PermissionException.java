package ru.tsc.panteleev.tm.exception.user;

import ru.tsc.panteleev.tm.exception.AbstractException;

public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Permission is incorrect ...");
    }

}
