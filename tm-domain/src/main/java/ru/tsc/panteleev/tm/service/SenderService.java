package ru.tsc.panteleev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.service.ISenderService;
import ru.tsc.panteleev.tm.dto.LogDTO;
import javax.jms.*;
import java.util.Date;

public class SenderService implements ISenderService {

    @NotNull
    private static final String TOPIC_NAME = "TM_TOPIC";

    @NotNull
    private final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);

    @Override
    @SneakyThrows
    public void send(@NotNull final LogDTO entity) {
        @NotNull final Connection connection = factory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(TOPIC_NAME);
        final MessageProducer producer = session.createProducer(destination);
        final ObjectMessage message = session.createObjectMessage(entity);
        producer.send(message);
        producer.close();
        session.close();
        connection.close();
    }

    @Override
    @SneakyThrows
    public LogDTO createMessage(@NotNull final Object object, @NotNull final String type) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        @NotNull final String className = object.getClass().getSimpleName();
        @NotNull final LogDTO message = new LogDTO(className, new Date().toString(), type, json);
        return message;
    }

}
