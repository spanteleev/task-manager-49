package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.LogDTO;

public interface ISenderService {

    void send(@NotNull LogDTO entity);

    LogDTO createMessage(@NotNull Object object, @NotNull String type);

}