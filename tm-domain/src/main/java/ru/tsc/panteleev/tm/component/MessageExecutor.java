package ru.tsc.panteleev.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.service.ISenderService;
import ru.tsc.panteleev.tm.dto.LogDTO;
import ru.tsc.panteleev.tm.service.SenderService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MessageExecutor {

    private static final int THREAD_COUNT = 3;

    @NotNull
    private final ISenderService service = new SenderService();

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);


    public void sendMessage(@NotNull final Object object, @NotNull final String type) {
        es.submit(() -> {
            @NotNull final LogDTO logDTO = service.createMessage(object, type);
            service.send(logDTO);
        });
    }

    public void stop() {
        es.shutdown();
    }

}
