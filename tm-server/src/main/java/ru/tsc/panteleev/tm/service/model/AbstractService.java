package ru.tsc.panteleev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.repository.model.IRepository;
import ru.tsc.panteleev.tm.api.service.IConnectionService;
import ru.tsc.panteleev.tm.api.service.model.IService;
import ru.tsc.panteleev.tm.model.AbstractModel;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
